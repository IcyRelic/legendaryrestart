package me.icyrelic.com;

import java.io.File;
import java.io.IOException;

import me.icyrelic.com.Commands.LRCommand;
import me.icyrelic.com.Commands.RestartCommand;
import me.icyrelic.com.Data.Updater;
import me.icyrelic.com.Data.Updater.UpdateResult;
import me.icyrelic.com.Data.Updater.UpdateType;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class LegendaryRestart extends JavaPlugin {
	public String prefix = (ChatColor.WHITE+"[" + ChatColor.GREEN + "LegendaryRestart" + ChatColor.WHITE + "] ");
	public String noPerm = (ChatColor.RED + "You Dont Have Permission!");
	boolean allowrestart = false;
	 
	public void onEnable(){
		
		
		ConsoleCommandSender console = getServer().getConsoleSender();
		if(getConfig().getBoolean("AutoUpdate")){
			Updater check = new Updater(this, 39616, this.getFile(), UpdateType.NO_DOWNLOAD, true);
			
			if (check.getResult() == UpdateResult.UPDATE_AVAILABLE) {
			    console.sendMessage(prefix+"New Version Available! "+ check.getLatestName());
			    
				Updater download = new Updater(this, 52950, this.getFile(), UpdateType.DEFAULT, true);
				
			    if(download.getResult() == UpdateResult.SUCCESS){
			    	console.sendMessage(prefix+"Successfully Updated Please Restart To Finalize");
			    }
			    
			}else{
				console.sendMessage(prefix+"You are currently running the latest version of LegendaryRestart");
			}
			


		}
		
		
		loadConfiguration();
		AutoRestart();
		File file;
		file = new File (getDataFolder()+"/restart.file");
        if (file.exists() ){
        	file.delete();
        }
        
        getCommand("restart").setExecutor(new RestartCommand(this));
        getCommand("legendaryrestart").setExecutor(new LRCommand(this));
	}
	
	
	public void onDisable(){

		
		
	}
	
	
	public void AutoRestart(){
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
		    public void run() {
		    	
		    	if(allowrestart){
		    		
		    		
		    			
		    			new Thread(new Runnable(){
							 
			                public void run() {
			                	
			                	if(getConfig().getBoolean("Settings.Automatic_Restarts.Broadcast")){
			                	getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.1").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
									Thread.sleep(180000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.2").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(60000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.3").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(30000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.4").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(15000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.5").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(5000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.6").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(5000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.7").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.8").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.9").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.10").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.11").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				getServer().broadcastMessage(getConfig().getString("Settings.Automatic_Restarts.Messages.12").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(500);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				
			                	}
			                	
			                	for(Player p : getServer().getOnlinePlayers()){
				 					
				 					
				 					p.kickPlayer(getConfig().getString("Settings.Restart_Message").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 					
				 				}
				 				
				 				File file;
				 				file = new File (getDataFolder()+"/restart.file");
				 		        if (! file.exists() ){
				 		        	try {
				 						file.createNewFile();
				 					} catch (IOException e) {
				 						// TODO Auto-generated catch block
				 						e.printStackTrace();
				 					}
				 		        }
				 				
				 				getServer().shutdown();
			                	
			                }
			 
			            }).start();						
		 				
		    			
	 				
	 				
	 				
	 				
	 				
		    	}else{
		    		allowrestart = true;
		    	}

		    }
		}, 0L, 1200*getConfig().getInt("Settings.Automatic_Restarts.Restart_Interval"));
		    		
		    		//30 mins 36000L
		    		//1 min 1200
		    }
	
	
	public void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	
	}
	
	
	
}
