package me.icyrelic.com.Commands;

import java.io.File;
import java.io.IOException;

import me.icyrelic.com.LegendaryRestart;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RestartCommand implements CommandExecutor {
	
	LegendaryRestart plugin;
	public RestartCommand(LegendaryRestart instance) {

		plugin = instance;

		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("restart")) {
			if(sender.hasPermission("LegendaryRestart.Restart")){
				
				
					 
					 new Thread(new Runnable(){
						 
			                public void run() {
			                	
			                	if(plugin.getConfig().getBoolean("Settings.Broadcast")){
			                	//Broadcast
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.1").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
									Thread.sleep(1000);
								} catch (InterruptedException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.2").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.3").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.4").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.5").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(1000);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				plugin.getServer().broadcastMessage(plugin.getConfig().getString("Settings.Messages.6").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 				try {
				 					Thread.sleep(500);
				 				} catch (InterruptedException e) {
				 					e.printStackTrace();
				 				}
				 				//end Broadcast
				 				
			                }	
				 				for(Player p : plugin.getServer().getOnlinePlayers()){
				 					
				 					
				 					p.kickPlayer(plugin.getConfig().getString("Settings.Restart_Message").replaceAll("(&([a-f0-9]))", "\u00A7$2"));
				 					
				 				}
				 				File file;
				 				file = new File (plugin.getDataFolder()+"/restart.file");
				 		        if (! file.exists() ){
				 		        	try {
				 						file.createNewFile();
				 					} catch (IOException e) {
				 						// TODO Auto-generated catch block
				 						e.printStackTrace();
				 					}
				 		        }
				 				
				 				plugin.getServer().shutdown();
			                	
			                	
			                }
			 
			            }).start();		
					 
	 				
	 				
					
					
 				
 				
 				
 				
				
			}else{
				sender.sendMessage(plugin.noPerm);
			}
			
		}
		return true;
	}

}
